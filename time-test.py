import time
import threading

sara_is_pedantic = 0

def printit():
  threading.Timer(2.0, printit).start()
  print("Hello, World! {}".format(sara_is_pedantic))
  global sara_is_pedantic
  sara_is_pedantic +=1
  if sara_is_pedantic == 4:
      print("OUR WORK HERE SHOULD BE DONE?")
      return

def printit_2():
  threading.Timer(5.0, printit_2).start()
  print("Hello, World! 5")
  
def delay(func, time):
    t_end = time.time() + time
    while time.time() < t_end:
        self.func()
    print("OUT OF LOOP")
    
def func(self):
    print("in LOOP")

    
if __name__ == "__main__":
    print("in main")
    length = 3
##    delay(func, length)
    printit()
##    printit_2()
    
    
    