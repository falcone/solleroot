## Sol LeRoot

This project investigates using robot primitives to create drawings in the style of Sol LeWitt.  The drawings are done using an off the shelf drawing robot, Root with custom appendages developed by the authors. The robots have no global reference frame and a narrow view of their environment, only being able to see a 3.5” wide row of color underneath the robot. As the robots draw, they stigmergicly augment their environment, affecting the path and decisions of future robots.  

## Fabricated attchements for the root

- https://www.thingiverse.com/thing:3364775
- https://www.thingiverse.com/thing:3364764

## Videos

[![](/images/fig_9.jpg)](https://youtu.be/h9NoDtw8VmM "Root multiline following")


[![](/images/fig_4.jpg)](https://youtu.be/L276xcI_x-w "Root painter test circle")


[![](/images/fig_5.jpg)](https://youtu.be/RIbZDw12SdM "Root within perimeter")

## Used these repos

- https://github.com/getsenic/gatt-python
- https://github.com/RootRobotics/root-robot-ble-protocol#ble-profile
- https://github.com/zlite/PyRoot
         
## Color decode chart for sensors

|*Color 1*|*Color 2*|*binary value*|*decimal value*|
|-----|------|----------|-----|
|White|	white|	00000000|	0 |
|Black|	white|	00010000|	16|
|Red  |	white|	00100000|	32|
|Green|	white|	00110000|	48|
|Blue |	white|	01000000|	64|
|	  |		 |          |     |
|White|	black|	00000001|	1 |
|Black|	black|	00010001|	17|
|Red  |	black|	00100001|	33|
|Green|	black|	00110001|	49|
|Blue |	black|	01000001|	65|
|     |      |          |     |
|White|	red |	00000010|	2 |
|Black|	red |	00010010|	18|
|Red|	red |	00100010|	34|
|Green|	red |	00110010|	50|
|Blue|	red |	01000010|	66|
|			|           |     |
|White|	green|	00000011|	3 |
|Black|	green|	00010011|	19|
|Red|	green|	00100011|	35|
|Green|	green|	00110011|	51|
|Blue|	green|	01000011|	67|
|	|		|           |     |
|White|	blue|	00000100|	4 |
|Black|	blue|	00010100|	20|
|Red|	blue|	00100100|	36|
|Green|	blue|	00110100|	52|
|Blue|	blue|	01000100|	68|


### Remember for use

turn on bluetooth

`sudo bluetoothctl` to talk to BlueZ
`power on`
`exit`

Install

[Bluetooth GATT SDK for python](https://github.com/getsenic/gatt-python)

